﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectTimerExportedXmlReader
{
    //class ListOfProjectsWeeklyEntryData
    //{
    //    private List<ProjectWeeklyEntryData> listPWED;

    //    public ListOfProjectsWeeklyEntryData()
    //    {
    //        // Empty constructor
    //    }
    //}

    class ProjectWeeklyEntryData
    {
        //private ProjectWeeklyEntryData pwed;

        public string year;
        public string week;
        public string name;
        public string priority;
        public string time;

        public ProjectWeeklyEntryData()
        {
            // Empty constructor
        }

        //public string GetName
        //{
        //    get { return name; }
            
        //}

        //public IEnumerator<string> GetEnumerator()
        //{
        //    return pwed.GetEnumerator();
        //}
    }

    //class ListOfExcelFileProjectLocations
    //{
    //    private List<ExcelFileProjectLocation> listEFPL;

    //    public ListOfExcelFileProjectLocations()
    //    {
    //        // Empty constructor
    //    }
    //}

    class ExcelFileProjectLocation
    {
        public string projectName;
        public int projectNameLocation;
        //public int[] yearsLocations = new int[10];
        public int firstWeekPriority1Location;

        public ExcelFileProjectLocation()
        {
            // Empty constructor
        }
    }
}
