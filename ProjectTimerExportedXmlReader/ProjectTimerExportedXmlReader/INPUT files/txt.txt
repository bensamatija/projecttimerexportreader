
Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 1	01/01/2018	Project Specific	Random Informative	Open	12,77
Overview	2018 week 1	01/01/2018	4 (u=L;i=L)	Loosing Time	Open	0,62
Overview	2018 week 1	02/01/2018	Project Specific	Customizing PC	Open	0,35
Overview	2018 week 1	01/01/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	0,69
Overview	2018 week 1	01/01/2018	3 (u=L;i=H)	Stocks Investment	Open	0,03
Overview	2018 week 1	01/01/2018	3 (u=L;i=H)	Reading (Books)	Open	4,66
Overview	2018 week 1	02/01/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	8,73
Overview	2018 week 1	01/01/2018	2 (u=H;i=L)	Software Programming (Learning)	Open	4,30
Overview	2018 week 1	06/01/2018	Project Specific	Hardware (Learning)	Open	0,28
Overview	2018 week 1				Total:	32,44

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 2	08/01/2018	1 (u=H;i=H)	Random Informative	Open	1,59
Overview	2018 week 2	08/01/2018	Project Specific	Random Informative	Open	9,98
Overview	2018 week 2	13/01/2018	4 (u=L;i=L)	Loosing Time	Open	1,03
Overview	2018 week 2	08/01/2018	3 (u=L;i=H)	Reading (Books)	Open	1,50
Overview	2018 week 2	08/01/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	0,44
Overview	2018 week 2	13/01/2018	1 (u=H;i=H)	Software Programming (Learning)	Open	7,96
Overview	2018 week 2				Total:	22,50

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 3	15/01/2018	4 (u=L;i=L)	Random Informative	Open	3,71
Overview	2018 week 3	15/01/2018	Project Specific	Random Informative	Open	2,76
Overview	2018 week 3	16/01/2018	4 (u=L;i=L)	Loosing Time	Open	0,30
Overview	2018 week 3	16/01/2018	Project Specific	Time Management	Open	0,18
Overview	2018 week 3	16/01/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	0,57
Overview	2018 week 3	17/01/2018	3 (u=L;i=H)	Reading (Books)	Open	2,00
Overview	2018 week 3	15/01/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	0,01
Overview	2018 week 3	15/01/2018	1 (u=H;i=H)	Software Programming (Learning)	Open	7,12
Overview	2018 week 3				Total:	16,66

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 4	27/01/2018	1 (u=H;i=H)	Random Informative	Open	1,06
Overview	2018 week 4	27/01/2018	3 (u=L;i=H)	Random Informative	Open	0,50
Overview	2018 week 4	25/01/2018	Project Specific	Random Informative	Open	7,72
Overview	2018 week 4	27/01/2018	4 (u=L;i=L)	Loosing Time	Open	0,63
Overview	2018 week 4	27/01/2018	1 (u=H;i=H)	Time Management	Open	1,00
Overview	2018 week 4	24/01/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	3,17
Overview	2018 week 4	26/01/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	6,88
Overview	2018 week 4	22/01/2018	1 (u=H;i=H)	Software Programming (Learning)	Open	12,28
Overview	2018 week 4				Total:	33,25

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 5	29/01/2018	1 (u=H;i=H)	Random Informative	Open	4,66
Overview	2018 week 5	03/02/2018	2 (u=H;i=L)	Random Informative	Open	0,85
Overview	2018 week 5	03/02/2018	3 (u=L;i=H)	Random Informative	Open	0,42
Overview	2018 week 5	29/01/2018	Project Specific	Random Informative	Open	6,37
Overview	2018 week 5	03/02/2018	1 (u=H;i=H)	Time Management	Open	1,17
Overview	2018 week 5	29/01/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	0,54
Overview	2018 week 5	03/02/2018	3 (u=L;i=H)	AI + NN + ML (machine learning)	Open	1,74
Overview	2018 week 5	29/01/2018	3 (u=L;i=H)	Reading (Books)	Open	2,83
Overview	2018 week 5	29/01/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	4,12
Overview	2018 week 5	03/02/2018	1 (u=H;i=H)	Software Programming (Learning)	Open	0,01
Overview	2018 week 5				Total:	22,72

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 6	06/02/2018	Project Specific	Random Informative	Open	11,40
Overview	2018 week 6	10/02/2018	4 (u=L;i=L)	Loosing Time	Open	1,97
Overview	2018 week 6	08/02/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	1,74
Overview	2018 week 6				Total:	15,11

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 7	15/02/2018	2 (u=H;i=L)	Random Informative	Open	1,69
Overview	2018 week 7	13/02/2018	4 (u=L;i=L)	Random Informative	Open	1,78
Overview	2018 week 7	12/02/2018	Project Specific	Random Informative	Open	2,09
Overview	2018 week 7	13/02/2018	4 (u=L;i=L)	Loosing Time	Open	0,32
Overview	2018 week 7	13/02/2018	1 (u=H;i=H)	Time Management	Open	8,36
Overview	2018 week 7	12/02/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	0,19
Overview	2018 week 7	12/02/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	1,26
Overview	2018 week 7				Total:	15,68

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 8	21/02/2018	1 (u=H;i=H)	Random Informative	Open	2,13
Overview	2018 week 8	23/02/2018	2 (u=H;i=L)	Random Informative	Open	2,81
Overview	2018 week 8	20/02/2018	3 (u=L;i=H)	Random Informative	Open	6,86
Overview	2018 week 8	19/02/2018	4 (u=L;i=L)	Random Informative	Open	2,90
Overview	2018 week 8	21/02/2018	1 (u=H;i=H)	Time Management	Open	4,38
Overview	2018 week 8	25/02/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	0,61
Overview	2018 week 8	24/02/2018	3 (u=L;i=H)	Our House Planning	Open	15,80
Overview	2018 week 8				Total:	35,49

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 9	26/02/2018	3 (u=L;i=H)	Random Informative	Open	4,44
Overview	2018 week 9	04/03/2018	3 (u=L;i=H)	Time Management	Open	1,07
Overview	2018 week 9	26/02/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	2,18
Overview	2018 week 9				Total:	7,69

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 10	05/03/2018	3 (u=L;i=H)	Random Informative	Open	0,31
Overview	2018 week 10	06/03/2018	4 (u=L;i=L)	Loosing Time	Open	0,24
Overview	2018 week 10	06/03/2018	3 (u=L;i=H)	Time Management	Open	0,49
Overview	2018 week 10	06/03/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	0,81
Overview	2018 week 10	10/03/2018	1 (u=H;i=H)	Reading (Books)	Open	1,00
Overview	2018 week 10	11/03/2018	3 (u=L;i=H)	Reading (Books)	Open	1,00
Overview	2018 week 10				Total:	3,86

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 11	12/03/2018	3 (u=L;i=H)	Random Informative	Open	21,09
Overview	2018 week 11	12/03/2018	4 (u=L;i=L)	Loosing Time	Open	0,72
Overview	2018 week 11	13/03/2018	1 (u=H;i=H)	Reading (Books)	Open	1,00
Overview	2018 week 11	12/03/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	2,88
Overview	2018 week 11				Total:	25,69

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 12	19/03/2018	3 (u=L;i=H)	Random Informative	Open	11,83
Overview	2018 week 12	22/03/2018	4 (u=L;i=L)	Random Informative	Open	8,06
Overview	2018 week 12	19/03/2018	Project Specific	Anything regarding the job	Open	0,55
Overview	2018 week 12				Total:	20,44

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 13	01/04/2018	1 (u=H;i=H)	Random Informative	Open	4,52
Overview	2018 week 13	26/03/2018	3 (u=L;i=H)	Random Informative	Open	24,22
Overview	2018 week 13	01/04/2018	4 (u=L;i=L)	Random Informative	Open	6,51
Overview	2018 week 13	27/03/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	15,83
Overview	2018 week 13				Total:	51,08

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 14	02/04/2018	1 (u=H;i=H)	Random Informative	Open	0,67
Overview	2018 week 14	02/04/2018	2 (u=H;i=L)	Random Informative	Open	4,56
Overview	2018 week 14	05/04/2018	3 (u=L;i=H)	Random Informative	Open	10,79
Overview	2018 week 14	02/04/2018	4 (u=L;i=L)	Random Informative	Open	0,00
Overview	2018 week 14	02/04/2018	4 (u=L;i=L)	Loosing Time	Open	10,41
Overview	2018 week 14	03/04/2018	1 (u=H;i=H)	Time Management	Open	3,07
Overview	2018 week 14	02/04/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	0,84
Overview	2018 week 14	05/04/2018	1 (u=H;i=H)	Software Programming (Learning)	Open	2,73
Overview	2018 week 14				Total:	33,07

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 15	11/04/2018	2 (u=H;i=L)	Random Informative	Open	3,39
Overview	2018 week 15	09/04/2018	3 (u=L;i=H)	Random Informative	Open	1,26
Overview	2018 week 15	13/04/2018	4 (u=L;i=L)	Loosing Time	Open	0,24
Overview	2018 week 15	15/04/2018	Project Specific	Customizing PC	Open	1,88
Overview	2018 week 15	11/04/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	6,70
Overview	2018 week 15	15/04/2018	1 (u=H;i=H)	Reading (Books)	Open	0,50
Overview	2018 week 15	09/04/2018	1 (u=H;i=H)	Software Programming (Learning)	Open	1,41
Overview	2018 week 15				Total:	15,38

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 16	16/04/2018	Project Specific	Customizing PC	Open	5,44
Overview	2018 week 16				Total:	5,44

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 17	24/04/2018	4 (u=L;i=L)	Loosing Time	Open	0,50
Overview	2018 week 17	25/04/2018	Project Specific	Customizing PC	Open	5,00
Overview	2018 week 17	25/04/2018	1 (u=H;i=H)	Reading (Books)	Open	0,97
Overview	2018 week 17				Total:	6,47

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 18	05/05/2018	2 (u=H;i=L)	Random Informative	Open	5,96
Overview	2018 week 18	06/05/2018	4 (u=L;i=L)	Loosing Time	Open	0,25
Overview	2018 week 18	06/05/2018	Project Specific	Customizing PC	Open	1,41
Overview	2018 week 18	04/05/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	12,12
Overview	2018 week 18	06/05/2018	1 (u=H;i=H)	Hardware (Learning)	Open	2,99
Overview	2018 week 18				Total:	22,73

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 19	07/05/2018	2 (u=H;i=L)	Random Informative	Open	7,28
Overview	2018 week 19	07/05/2018	4 (u=L;i=L)	Loosing Time	Open	1,70
Overview	2018 week 19	08/05/2018	Project Specific	Customizing PC	Open	0,00
Overview	2018 week 19	09/05/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	4,12
Overview	2018 week 19	11/05/2018	1 (u=H;i=H)	Reading (Books)	Open	2,51
Overview	2018 week 19	07/05/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	0,32
Overview	2018 week 19	07/05/2018	1 (u=H;i=H)	Hardware (Learning)	Open	2,52
Overview	2018 week 19				Total:	18,46

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 20	20/05/2018	1 (u=H;i=H)	Random Informative	Open	1,26
Overview	2018 week 20	18/05/2018	2 (u=H;i=L)	Random Informative	Open	6,43
Overview	2018 week 20	19/05/2018	4 (u=L;i=L)	Loosing Time	Open	1,10
Overview	2018 week 20	19/05/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	4,63
Overview	2018 week 20	15/05/2018	1 (u=H;i=H)	SolidWorks, Design (Learning)	Open	21,76
Overview	2018 week 20				Total:	35,18

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 21	21/05/2018	2 (u=H;i=L)	Random Informative	Open	0,40
Overview	2018 week 21	23/05/2018	3 (u=L;i=H)	Random Informative	Open	0,61
Overview	2018 week 21	24/05/2018	3 (u=L;i=H)	Loosing Time	Open	0,87
Overview	2018 week 21	23/05/2018	4 (u=L;i=L)	Loosing Time	Open	0,31
Overview	2018 week 21	21/05/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	3,36
Overview	2018 week 21				Total:	5,54

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 22	03/06/2018	3 (u=L;i=H)	Loosing Time	Open	1,10
Overview	2018 week 22				Total:	1,10

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 23	04/06/2018	3 (u=L;i=H)	Random Informative	Open	0,94
Overview	2018 week 23	04/06/2018	4 (u=L;i=L)	Loosing Time	Open	2,57
Overview	2018 week 23	04/06/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	5,42
Overview	2018 week 23				Total:	8,93

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 24	12/06/2018	2 (u=H;i=L)	Random Informative	Open	4,55
Overview	2018 week 24	17/06/2018	4 (u=L;i=L)	Random Informative	Open	0,41
Overview	2018 week 24	12/06/2018	4 (u=L;i=L)	Loosing Time	Open	3,32
Overview	2018 week 24	13/06/2018	2 (u=H;i=L)	Customizing PC	Open	4,07
Overview	2018 week 24	12/06/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	4,48
Overview	2018 week 24				Total:	16,84

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 25	21/06/2018	1 (u=H;i=H)	Random Informative	Open	0,91
Overview	2018 week 25	19/06/2018	2 (u=H;i=L)	Random Informative	Open	4,60
Overview	2018 week 25	19/06/2018	3 (u=L;i=H)	Random Informative	Open	1,30
Overview	2018 week 25	20/06/2018	4 (u=L;i=L)	Loosing Time	Open	2,84
Overview	2018 week 25	20/06/2018	2 (u=H;i=L)	Customizing PC	Open	4,02
Overview	2018 week 25	21/06/2018	1 (u=H;i=H)	Reading (Books)	Open	2,07
Overview	2018 week 25				Total:	15,74

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 26	28/06/2018	1 (u=H;i=H)	Random Informative	Open	3,18
Overview	2018 week 26	28/06/2018	2 (u=H;i=L)	Random Informative	Open	2,13
Overview	2018 week 26	26/06/2018	3 (u=L;i=H)	Random Informative	Open	3,37
Overview	2018 week 26	25/06/2018	4 (u=L;i=L)	Loosing Time	Open	2,30
Overview	2018 week 26	29/06/2018	2 (u=H;i=L)	Customizing PC	Open	2,20
Overview	2018 week 26	26/06/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	5,86
Overview	2018 week 26	29/06/2018	2 (u=H;i=L)	Software Programming (Learning)	Open	9,77
Overview	2018 week 26				Total:	28,82

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 27	04/07/2018	1 (u=H;i=H)	Random Informative	Open	0,16
Overview	2018 week 27	02/07/2018	2 (u=H;i=L)	Random Informative	Open	0,58
Overview	2018 week 27	03/07/2018	4 (u=L;i=L)	Loosing Time	Open	1,03
Overview	2018 week 27	02/07/2018	2 (u=H;i=L)	Customizing PC	Open	9,94
Overview	2018 week 27	02/07/2018	1 (u=H;i=H)	Reading (Books)	Open	0,50
Overview	2018 week 27				Total:	12,20

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 28	09/07/2018	4 (u=L;i=L)	Loosing Time	Open	0,58
Overview	2018 week 28	15/07/2018	1 (u=H;i=H)	Soft Skills (Learning)	Open	3,88
Overview	2018 week 28	15/07/2018	1 (u=H;i=H)	Software Programming (Learning)	Open	5,37
Overview	2018 week 28				Total:	9,83

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 29	16/07/2018	1 (u=H;i=H)	Random Informative	Open	3,48
Overview	2018 week 29	20/07/2018	3 (u=L;i=H)	Random Informative	Open	0,25
Overview	2018 week 29	19/07/2018	4 (u=L;i=L)	Loosing Time	Open	2,47
Overview	2018 week 29	16/07/2018	1 (u=H;i=H)	Software Programming (Learning)	Open	7,57
Overview	2018 week 29	16/07/2018	1 (u=H;i=H)	Konstruiranje Jekla	Open	5,92
Overview	2018 week 29	19/07/2018	2 (u=H;i=L)	Konstruiranje Jekla	Open	1,23
Overview	2018 week 29				Total:	20,91

Project	Period	Date	Task	Project	Status	Hours
Overview	2018 week 30	24/07/2018	1 (u=H;i=H)	Random Informative	Open	2,63
Overview	2018 week 30	23/07/2018	4 (u=L;i=L)	Loosing Time	Open	3,59
Overview	2018 week 30	25/07/2018	1 (u=H;i=H)	Time Management	Open	1,33
Overview	2018 week 30	24/07/2018	1 (u=H;i=H)	AI + NN + ML (machine learning)	Open	0,57
Overview	2018 week 30	23/07/2018	1 (u=H;i=H)	Software Programming (Learning)	Open	10,13
Overview	2018 week 30				Total:	18,26
