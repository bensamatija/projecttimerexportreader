﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Xml;
using System.IO;
using EXCEL = Microsoft.Office.Interop.Excel;
//using OOX = OfficeOpenXml;      // EPPlus.dll
using System.Runtime.InteropServices;


namespace ProjectTimerExportedXmlReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Title = "Project Timer 2 XLS Importer";
        }
        
        public string dataInputPath = @"C:\Users\Matija\Desktop\INPUT files\txt.txt";
        public string dataOutputPath = @"C:\Users\Matija\Desktop\INPUT files\PTxls.xls";
        //public string dataInputPath = @"INPUT files\txt.txt";
        //public string dataOutputPath = @"INPUT files\PTxls.xls";

        // Create new Empty list of PWED objects:
        List<ProjectWeeklyEntryData> listPWED = new List<ProjectWeeklyEntryData>();

        // Create new Empty list of Project Names:
        List<string> listProjectNames = new List<string>();

        // Create new Empty List of EFLP:
        //ExcelFileProjectLocation EFPL = new ExcelFileProjectLocation();
        List<ExcelFileProjectLocation> listEFPL = new List<ExcelFileProjectLocation>();

        // Define the relative position of the values per each line here:
        // "Project, Period, Date, Task, Project, Status, Hours"
        int[] neededElements = { 4, 1, 3, 6 };
        string[] dateParts;
        string[] priorityParts;

        string line;

        EXCEL._Worksheet xlWorksheet;
        EXCEL.Application xlApp;
        EXCEL.Workbooks xlWorkbooks;
        EXCEL.Workbook xlWorkbook;
        EXCEL.Range xlRange;

        int rowCount;
        int colCount;

        int currentYear_row;
        //int year2017_week1_row;
        //int year2018_week1_row;


        private void Import()
        {
            listPWED = new List<ProjectWeeklyEntryData>();
            listProjectNames = new List<string>();
            listEFPL = new List<ExcelFileProjectLocation>();

            SaveSettings();
            LoadSettings();     // In case the app wasn't restarted after change
            ReadFile();
            GetExcelFile();
            PredefinedProjectCells();
            FindProjectCell();
            WriteDataIntoExcel();
            //TestWrite();

            CleanExcellFromMemory();
        }

        private void TestWrite()
        {
            xlWorksheet.Cells[
                            1,                  // ROW => x
                            2].Value            // COL => y
                            = "Test";
        }

        private void PredefinedProjectCells()
        {
            //year2017_week1_row = 3;
            //year2018_week1_row = 58;
            currentYear_row = 58;


        }

        private void ReadFile()
        {
            using (StreamReader sr = new StreamReader(dataInputPath))
            {
                // Read and display lines from the file until the end of 
                // the file is reached.
                while ((line = sr.ReadLine()) != null)

                {
                    ProcesReadedLine(line);
                }
            }
        }

        private void ProcesReadedLine(string line)
        {
            if (line.Contains("Overview") && (!line.Contains("Total")))
            {
                // Create new object:
                ProjectWeeklyEntryData PWED = new ProjectWeeklyEntryData();

                // Split the line:
                string[] lineParts = line.Split('\t');

                // Split date to year and week:
                dateParts = lineParts[1].Split(' ');

                // Split project priority:
                priorityParts = lineParts[3].Split(' ');

                // Write to object:
                PWED.name = lineParts[4];
                PWED.priority = priorityParts[0];
                PWED.year = dateParts[0];
                PWED.week = dateParts[2];
                PWED.time = lineParts[6];

                // Add PWED to the list:
                listPWED.Add(PWED);

                // Add Project Name to the list:
                listProjectNames = AddToListOfProjectNames(PWED.name);
            }
        }

        private void FindProjectCell()
        {
            rowCount = xlRange.Rows.Count;
            colCount = xlRange.Columns.Count;

            int n = 0;

            foreach (var projectName in listProjectNames)
            {
                ExcelFileProjectLocation EFPL = new ExcelFileProjectLocation();
                // Write to object:
                EFPL.projectName = projectName;
                EFPL.projectNameLocation = Find_ProjectColumn_Name_Location(projectName);
                EFPL.firstWeekPriority1Location = currentYear_row;

                listEFPL.Add(EFPL);
                n++;
            }

        }

        private void WriteDataIntoExcel()
        {
            int priorityPositionOffset = 0;
            int weekPositionOffset = 0;

            foreach (var project in listEFPL)
            {
                foreach (var entry in listPWED)
                {
                    if (project.projectName == entry.name)
                    {
                        // Define offset depending on the Priority:
                        if (int.TryParse(entry.priority, out int p)) { priorityPositionOffset = p - 1; }
                        else { priorityPositionOffset = 3; }

                        // Define offset depending on the Week:
                        weekPositionOffset = int.Parse(entry.week) - 1;

                        // For Searching the value:
                        if (entry.time.StartsWith("0,69"))
                        { }

                        //if (project.projectNameLocation + priorityPositionOffset > )

                        // Check first if data exists on the cell we are gonna write to.
                        // If it does sum the value
                        string tempValue = Convert.ToString((xlWorksheet.Cells[project.firstWeekPriority1Location + weekPositionOffset, project.projectNameLocation + priorityPositionOffset].Value));
                        string theValue = "N/A";

                        if ((tempValue != null) && (tempValue != ""))
                        {
                            //tempValue = Convert.ToString( (xlWorksheet.Cells[project.firstWeekPriority1Location + weekPositionOffset, project.projectNameLocation + priorityPositionOffset].Value));

                            if ((float.TryParse(entry.time, out float t)) && (float.TryParse(tempValue, out float v)))
                            {
                                theValue = ((float)t + (float)v).ToString();
                            }
                        }
                        else
                        { theValue = entry.time; }

                        // Convert the value to the number so excel can easily operate with it in mathematical functions
                        float theValueFloat = -1.0f;
                        if (float.TryParse(theValue, out float d))
                        {
                            theValueFloat = d;
                        }
                        else
                        {
                            MessageBox.Show("Error converting from string to float."); 
                        }

                        // Write data into cell:
                        xlWorksheet.Cells[
                            project.firstWeekPriority1Location + weekPositionOffset,
                            project.projectNameLocation + priorityPositionOffset].Value
                            = theValueFloat;
                    }
                }
            }
        }

        //private int Find_ProjectRow_Year(string projectName)
        //{
        //    int year = 0;

        //    int r = 1;
        //    int c = 1;
        //    string cellValue;

        //    // Check if the Project name exists in the excel Row A:
        //    for (r = 1; r <= colCount; r++)
        //    {
        //        if (xlRange.Cells[r, c] != null && xlRange.Cells[r, c].Value2 != null)
        //        {
        //            cellValue = xlWorksheet.Cells[r, c].Value.ToString();

        //            //foreach (var p in listProjectNames)

        //            // Parse the string to int:  

        //            if (int.TryParse(cellValue, out year))
        //            {
        //                //ExcelFileProjectLocation EFPL = new ExcelFileProjectLocation();
        //                //EFPL.projectLocation = r;
        //                //listEFPL.Add()
        //                return year;
        //            }

        //        }
        //    }
        //    return year;
        //}

        private int Find_ProjectColumn_Name_Location(string projectName)
        {
            int r = 1;
            int c = 1;
            string cellValue;

            // Check if the Project name exists in the excel Row A:
            for (c = 1; c <= colCount; c++)
            {
                if (xlRange.Cells[r, c] != null && xlRange.Cells[r, c].Value2 != null)
                {
                    cellValue = xlWorksheet.Cells[r, c].Value.ToString();

                    //foreach (var p in listProjectNames)
                    {
                        // Project name in 1st row in Excel matched with the name in list of project names:                      
                        if (cellValue == projectName)
                        {
                            return c;
                        }
                    }
                }
            }
            return c;
        }

        /// <summary>
        /// Add project name to the list if it doesn't exist there yet
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private List<string> AddToListOfProjectNames(string name)
        {
            if (listProjectNames != null)
            {
                foreach (var n in listProjectNames)
                {
                    if (n == name)
                    {
                        return listProjectNames;
                    }
                }
                listProjectNames.Add(name);
            }
            return listProjectNames;
        }

        private void GetExcelFile()
        {
            //Create COM Objects. Create a COM object for everything that is referenced
            xlApp = new EXCEL.Application();
            //EXCEL.Workbook xlWorkbook = xlApp.Workbooks.Open(dataOutputPath);
            xlWorkbooks = xlApp.Workbooks;
            xlWorkbook = xlWorkbooks.Open(dataOutputPath);
            xlWorksheet = xlWorkbook.Sheets[1];        // sheet 1
            xlRange = xlWorksheet.UsedRange;
        }

        private void CleanExcellFromMemory()
        {
            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //rule of thumb for releasing com objects:
            //  never use two dots, all COM objects must be referenced and released individually
            //  ex: [somthing].[something].[something] is bad

            xlWorkbook.Save();

            //release com objects to fully kill excel process from running in the background
            xlWorkbook.Close();

            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            Marshal.ReleaseComObject(xlWorkbook);
            Marshal.ReleaseComObject(xlWorkbooks);

            //close and release
            //xlWorkbook.Close();
            //Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.txtFile = tb_exportedTxtFile.Text;
            Properties.Settings.Default.xlsFile = tb_excelXlsFile.Text;
            Properties.Settings.Default.Save();
        }

        private void LoadSettings()
        {
            tb_exportedTxtFile.Text = Properties.Settings.Default.txtFile;
            dataInputPath = Properties.Settings.Default.txtFile;
            tb_excelXlsFile.Text = Properties.Settings.Default.xlsFile;
            dataOutputPath = Properties.Settings.Default.xlsFile;
        }

        private void btn_Read_Click(object sender, RoutedEventArgs e)
        {
            Import();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveSettings();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadSettings();
        }
    }
}
